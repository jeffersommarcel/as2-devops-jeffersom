import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import ListUser from '../../views/pages/ListUser.vue'

describe('ListUser', () => {
  it('should emit "delete-user" when delete button click', () => {
    const wrapper = mount(ListUser, {
        props: {
            users: [{name: 'teste', lastName: 'teste2', email: 'teste3'},]
        }
    });
    wrapper.find('button').trigger('click');

    expect(wrapper.emitted()).toHaveProperty('delete-user')
  })
})
