import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import RegisterPage from '../../views/pages/RegisterPage.vue'

describe('RegisterPage', () => {
  it('should not render ListUser component when has not users', () => {
    const wrapper = mount(RegisterPage);
    const listUser = wrapper.findComponent({name: 'ListUser'});

    expect(listUser.exists()).toBe(false);
  })
})
